﻿using Data.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public class OKRDetail: IEntity
    {
        public int ID { get; set; }
        public int CategoryID { get; set; }
        public string Content { get; set; }
        public int OKRID { get; set; }
    
     

    }
}
