﻿using Data.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public class OC: IEntity
    {
        public OC()
        {
            CreatedDate = DateTime.Now;
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public double Level { get; set; }
        public int ParentID { get; set; }
        public string URL { get; set; }
        public string Path { get; set; }
        public int UserID { get; set; }
        //public int TaskID { get; set; }
        public DateTime CreatedDate { get; set; }
        public virtual Task Task { get; set; }
        public virtual List<Manager> Managers { get; set; }
        //public virtual List<TeamMember> TeamMembers { get; set; }

    }
}
