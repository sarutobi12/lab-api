﻿using Data.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
   public class Project: IEntity
    {

        public Project()
        {
            CreatedDate = DateTime.Now;
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string URL { get; set; }
        public string WorkBy { get; set; }
        public DateTime CreatedDate { get; set; }
        //public virtual List<Manager> Managers { get; set; }
        //public virtual List<TeamMember> TeamMembers { get; set; }

    }
}
