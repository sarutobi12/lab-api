﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.ViewModel.Project
{
   public class ProjectViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string URL { get; set; }
        public string WorkBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
