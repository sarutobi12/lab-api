﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.ViewModel.Slide
{
   public class SlideViewModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string Subtitle { get; set; }
        //public string WorkBy { get; set; }
    }
}
