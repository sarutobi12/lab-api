﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.ViewModel.OKRDetail
{
   public class OKRDetailViewModel
    {
        public int ID { get; set; }
        public int CategoryID { get; set; }
        public string Content { get; set; }
        public int OKRID { get; set; }
        public string arrContent { get; set; }
        public string arrID { get; set; }

    }
}
