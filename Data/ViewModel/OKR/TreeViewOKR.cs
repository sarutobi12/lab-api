﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data.ViewModel.OKR
{
    public class TreeViewOKR
    {
        public TreeViewOKR()
        {
            this.children = new List<TreeViewOKR>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public int ParentID { get; set; }

        public bool HasChildren
        {
            get { return children.Any(); }
        }

        public List<TreeViewOKR> children { get; set; }
    }
}
