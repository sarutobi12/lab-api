﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.ViewModel.OKR
{
   public class OKRViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public int Parentid { get; set; }

        public int CategoryID { get; set; }
        public string Content { get; set; }
        public int OKRID { get; set; }
        public string CategoryName { get; set; }
        public List<OKRConfidence> OKRConfidences { get; set; }

        public class OKRConfidence
        {
            public int ID { get; set; }
            public int CategoryID { get; set; }
            public string Content { get; set; }
            public int OKRID { get; set; }
        }
    }
}
