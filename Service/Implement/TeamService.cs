﻿using AutoMapper;
using Data;
using Data.Models;
using Data.ViewModel.TeamMember;
using Microsoft.EntityFrameworkCore;
using Service.Helpers;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Implement
{
    public class TeamService : ITeamService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        //public static IWebHostEnvironment _environment;
        public TeamService(DataContext context , IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            //_environment = environment;
        }

        public async Task<bool> Create(TeamMember entity)
        {
            await _context.TeamMembers.AddAsync(entity);

            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;

            }

          
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _context.TeamMembers.FindAsync(id);
            if (entity == null)
            {
                return false;
            }

             _context.TeamMembers.Remove(entity);
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }

        public async Task<List<TeamMember>> GetAll()
        {
            return await _context.TeamMembers.OrderBy(x=>x.Position).ToListAsync();
        }
        public async Task<List<TeamMemberViewModel>> GetListMember()
        {
            return _mapper.Map<List<TeamMemberViewModel>>(await _context.TeamMembers.ToListAsync());
        }

        public async Task<PagedList<TeamMember>> GetAllPaging(string keyword, int page, int pageSize)
        {
            var source = _context.TeamMembers.AsQueryable();
            if (!keyword.IsNullOrEmpty())
            {
                source = source.Where(x => x.Name.Contains(keyword));
            }
            return await PagedList<TeamMember>.CreateAsync(source, page, pageSize);
        }

        public async Task<TeamMember> GetByID(int id)
        {
            return await _context.TeamMembers.FindAsync(id);
        }

        public async Task<bool> Update(TeamMember entity)
        {
           var item= await _context.TeamMembers.FindAsync(entity.ID);
            item.Name = entity.Name;
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }
    }
}
