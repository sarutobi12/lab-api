﻿using AutoMapper;
using Data;
using Data.Models;
using Data.ViewModel.OKR;
using Data.ViewModel.OKRCategory;
using Data.ViewModel.OKRDetail;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Data.ViewModel.OKR.OKRViewModel;

namespace Service.Implement
{
    public class OKRService : IOkrService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public OKRService(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Task<bool> AddOrUpdate(OC entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<TreeView>> GetListTree()
        {
            var listLevels = await _context.OKRs.OrderBy(x => x.Level).ToListAsync();
            var levels = new List<TreeView>();
            foreach (var item in listLevels)
            {
                var levelItem = new TreeView();
                levelItem.key = item.ID;
                levelItem.title = item.Name;
                levelItem.levelnumber = item.Level;
                levelItem.parentid = item.Parentid;
                levels.Add(levelItem);
            }

            List<TreeView> hierarchy = new List<TreeView>();

            hierarchy = levels.Where(c => c.parentid == 0)
                            .Select(c => new TreeView()
                            {
                                key = c.key,
                                title = c.title,
                                code = c.code,
                                levelnumber = c.levelnumber,
                                parentid = c.parentid,
                                link = c.link,
                                index = c.index,
                                linkpath = c.linkpath,
                                children = GetChildren(levels, c.key)
                            })
                            .ToList();


            HieararchyWalk(hierarchy);
            int index = 1;
            hierarchy.ForEach(item =>
            {
                if (item.children.Count >= 0 && item.parentid == 0)
                {
                    item.index = index.ToString();
                    index++;
                }
            });
            return hierarchy;
        }
        private void HieararchyWalk(List<TreeView> hierarchy)
        {
            if (hierarchy != null)
            {
                foreach (var item in hierarchy)
                {
                    //Console.WriteLine(string.Format("{0} {1}", item.Id, item.Text));
                    HieararchyWalk(item.children);
                }
            }
        }
        public List<TreeView> GetChildren(List<TreeView> levels, int parentid)
        {
            return levels
                    .Where(c => c.parentid == parentid)
                    .Select(c => new TreeView()
                    {
                        key = c.key,
                        title = c.title,
                        code = c.code,
                        levelnumber = c.levelnumber,
                        parentid = c.parentid,
                        link = c.link,
                        linkpath = c.linkpath,
                        children = GetChildren(levels, c.key)
                    })
                    .ToList();
        }

        public string GetNode(string code)
        {
            throw new NotImplementedException();

        }

        public string GetNode(int id)
        {
            var list = new List<OC>();
            list = _context.OCs.ToList();
            var list2 = new List<OC>();
            list2.Add(list.FirstOrDefault(x => x.ID == id));
            var parentID = list.FirstOrDefault(x => x.ID == id).ParentID;
            foreach (var item in list)
            {
                if (parentID == 0)
                    break;
                if (parentID != 0)
                {
                    //add vao list1
                    list2.Add(list.FirstOrDefault(x => x.ID == parentID));
                }
                //cap nhat lai parentID
                parentID = list.FirstOrDefault(x => x.ID == parentID).ParentID;

            }
            return string.Join("->", list2.OrderBy(x => x.ParentID).Select(x => x.Name).ToArray());
        }

        public async Task<bool> IsExistsCode(int ID)
        {
            return await _context.OCs.AnyAsync(x => x.ID == ID);
        }

        public async Task<bool> Rename(TreeViewRenameOKR level)
        {
            var item = await _context.OKRs.FindAsync(level.key);
            item.Name = level.title;
            
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public async Task<IEnumerable<TreeViewOKR>> GetListTreeOC(int parentID, int id)
        {
            var listLevels = await _context.OKRs.OrderBy(x => x.Level).ToListAsync();
            var levels = new List<TreeViewOKR>();
            foreach (var item in listLevels)
            {
                var levelItem = new TreeViewOKR();
                levelItem.ID = item.ID;
                levelItem.Name = item.Name;
                levelItem.Level = item.Level;
                levelItem.ParentID = item.Parentid;
                levels.Add(levelItem);
            }

            List<TreeViewOKR> hierarchy = new List<TreeViewOKR>();

            hierarchy = levels.Where(c => c.ID == id && c.ParentID == parentID)
                            .Select(c => new TreeViewOKR()
                            {
                                ID = c.ID,
                                Name = c.Name,
                                Level = c.Level,
                                ParentID = c.ParentID,
                                children = GetTreeChildren(levels, c.ID)
                            })
                            .ToList();


            HieararchyWalkTree(hierarchy);

            return hierarchy;
        }

        private void HieararchyWalkTree(List<TreeViewOKR> hierarchy)
        {
            if (hierarchy != null)
            {
                foreach (var item in hierarchy)
                {
                    //Console.WriteLine(string.Format("{0} {1}", item.Id, item.Text));
                    HieararchyWalkTree(item.children);
                }
            }
        }

        public List<TreeViewOKR> GetTreeChildren(List<TreeViewOKR> levels, int parentid)
        {
            return levels
                    .Where(c => c.ParentID == parentid)
                    .Select(c => new TreeViewOKR()
                    {
                        ID = c.ID,
                        Name = c.Name,
                        Level = c.Level,
                        ParentID = c.ParentID,
                        children = GetTreeChildren(levels, c.ID)
                    })
                    .ToList();
        }

        public async Task<object> CreateOC(CreateOKRViewModel oc)
        {
            try
            {
                var item = _mapper.Map<Data.Models.OKR>(oc);
                item.Level = 1;

                await _context.OKRs.AddAsync(item);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        public async Task<object> CreateSubOC(CreateOKRViewModel oc)
        {
            var item = _mapper.Map<Data.Models.OC>(oc);

            //Level cha tang len 1 va gan parentid cho subtask
            var taskParent = _context.OKRs.Find(item.ParentID);
            item.Level = taskParent.Level + 1;
            item.ParentID = oc.Parentid;
            await _context.OCs.AddAsync(item);
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int ID)
        {
            var item = await _context.OKRs.FindAsync(ID);
            var item2 = await _context.OKRDetails.ToListAsync();
            foreach (var items in item2)
            {
                if (items.OKRID == ID)
                _context.OKRDetails.RemoveRange(await _context.OKRDetails.FirstOrDefaultAsync(x => x.OKRID == ID));
            }
            _context.OKRs.Remove(item);
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public async Task<bool> DeleteOkrDetail(int ID)
        {
            var entity = await _context.OKRDetails.FindAsync(ID);
            if (entity == null)
            {
                return false;
            }

            _context.OKRDetails.Remove(entity);
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }


        public async Task<object> AddDetailOKR(OKRDetailViewModel entity)
        {

            var list = new List<OKRDetail>();

            foreach (var item in entity.arrContent.Split("@@##"))
            {
                if(item != "")
                list.Add(new OKRDetail { 
                    CategoryID = entity.CategoryID ,
                    OKRID = entity.OKRID ,
                    Content = item 
                });
            }

            try
            {
                await _context.AddRangeAsync(list);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public async Task<List<OKRCategoryViewModel>> GetListOKRCategory()
        {
            return _mapper.Map<List<OKRCategoryViewModel>>(await _context.OKRCategory.ToListAsync());
        }

        public async Task<object> GetDetail(int okrID)
        {
            try
            {
                var model = await (from okrdetail in _context.OKRDetails
                                   where okrdetail.OKRID == okrID
                                   join okr in _context.OKRs on okrdetail.OKRID equals okr.ID
                                   join okrcategory in _context.OKRCategory on okrdetail.CategoryID equals okrcategory.ID
                                   select new OKRViewModel
                                   {
                                       ID = okrdetail.ID,
                                       Name = okr.Name,
                                       //Level = okr.Level,
                                       //Parentid = okr.Parentid,
                                       CategoryID = okrdetail.CategoryID,
                                       CategoryName = okrcategory.Name,
                                       Content = okrdetail.Content
                                   }).ToListAsync();
                var Confidences = new List<OKRConfidence>();
                var TeamHealth = new List<OKRConfidence>();
                var Progress = new List<OKRConfidence>();
                var NextWeek = new List<OKRConfidence>();

                Confidences = model.Where(x => x.CategoryID == 1).Select(x => new OKRConfidence
                {
                    ID = x.ID,
                    Content = x.Content
                }).ToList();
                TeamHealth = model.Where(x => x.CategoryID == 2).Select(x => new OKRConfidence
                {
                    ID = x.ID,

                    Content = x.Content
                }).ToList();
                Progress = model.Where(x => x.CategoryID == 3).Select(x => new OKRConfidence
                {
                    ID = x.ID,

                    Content = x.Content
                }).ToList();
                NextWeek = model.Where(x => x.CategoryID == 4).Select(x => new OKRConfidence
                {
                    ID = x.ID,

                    Content = x.Content
                }).ToList();

                return new 
                {
                    //data = model,
                    Confidence = Confidences,
                    Teamhealth = TeamHealth,
                    Progess = Progress,
                    Nextweek = NextWeek


                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                };
            }
        }

        public async Task<object> GetListAllCategory(int okrID)
        {
            var model = await _context.OKRCategory.Select(x => new
            {
                x.ID,
                x.Name,
            }).ToListAsync();
            return model;
        }


        public async Task<object> UpdateOKR(OKRDetailViewModel entity)
        {
            try
            {
                //var update = await _context.OKRDetails.Where(x => x.OKRID == entity.ID && x.CategoryID == entity.CategoryID).ToListAsync();

                var aa = new List<OKRDetail>();

                foreach (var item in entity.arrID.Split("@@##").Select((value, i) => new { i, value }))
                {
                   
                    if(item.value != "")
                    {
                    aa.Add(new OKRDetail { ID = Convert.ToInt32(item.value), OKRID = item.i });
                    }

                }

                var aa2 = new List<OKRDetail>();

                foreach (var item in entity.arrContent.Split("@@##").Select((value, i) => new { i, value }))
                {
                    if (item.value != "")
                    {
                        aa2.Add(new OKRDetail { Content = item.value, OKRID = item.i });
                    }
                }

                var aa3 = new List<OKRDetail>();

                
                //GAN id CHO CONENT
                foreach (var item in aa)
                {
                    foreach (var item2 in aa2)
                    {
                        if (item.OKRID == item2.OKRID)
                        {
                            aa3.Add(new OKRDetail { Content = item2.Content, ID = item.ID });
                            //item.Content = item2.Content;
                        }
                    }
                }

                var aaa = new List<OKRDetail>();

                foreach (var item in aa3)
                {
                   if(item.ID == 0)
                    {
                        aaa.Add(new OKRDetail
                        {
                            CategoryID = entity.CategoryID,
                            OKRID = entity.OKRID,
                            Content = item.Content
                        });
                    }
                }
                await _context.AddRangeAsync(aaa);
                await _context.SaveChangesAsync();

                var list = await _context.OKRDetails.Where(x => aa3.Select(y => y.ID).Contains(x.ID)).ToListAsync();

                foreach (var item in list)
                {
                   
                    foreach (var item2 in aa3)
                    {
                        if (item.ID == item2.ID)
                        {
                            item.Content = item2.Content;
                        }
                    }
                }
                await _context.SaveChangesAsync();

               
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

     
        }

    }
}
