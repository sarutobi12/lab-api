﻿using AutoMapper;
using Data;
using Data.Models;
using Data.ViewModel.Slide;
using Microsoft.EntityFrameworkCore;
using Service.Helpers;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Implement
{
    public class SlideService : ISlideService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        //public static IWebHostEnvironment _environment;
        public SlideService(DataContext context , IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            //_environment = environment;
        }

        public async Task<bool> Create(Slide entity)
        {
            await _context.Slides.AddAsync(entity);

            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;

            }

          
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _context.Slides.FindAsync(id);
            if (entity == null)
            {
                return false;
            }

             _context.Slides.Remove(entity);
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }

        public async Task<List<Slide>> GetAll()
        {
            return await _context.Slides.ToListAsync();
        }
        public async Task<List<SlideViewModel>> GetListSlide()
        {
            return _mapper.Map<List<SlideViewModel>>(await _context.Slides.ToListAsync());
        }

        public async Task<PagedList<Slide>> GetAllPaging(string keyword, int page, int pageSize)
        {
            var source = _context.Slides.AsQueryable();
            if (!keyword.IsNullOrEmpty())
            {
                source = source.Where(x => x.Title.Contains(keyword));
            }
            return await PagedList<Slide>.CreateAsync(source, page, pageSize);
        }

        public async Task<Slide> GetByID(int id)
        {
            return await _context.Slides.FindAsync(id);
        }

        public async Task<bool> Update(Slide entity)
        {
           var item= await _context.Slides.FindAsync(entity.ID);
            item.Title = entity.Title;
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }
    }
}
