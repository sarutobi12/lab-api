﻿using AutoMapper;
using Data;
using Data.Models;
using Data.ViewModel.Project;
using Microsoft.EntityFrameworkCore;
using Service.Helpers;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Implement
{
    public class ArticleService : IArticleService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public ArticleService(DataContext context , IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> Create(Article entity)
        {
            await _context.Articles.AddAsync(entity);

            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _context.Articles.FindAsync(id);
            if (entity == null)
            {
                return false;
            }

             _context.Articles.Remove(entity);
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }

        public async Task<List<Article>> GetAll()
        {
            return await _context.Articles.ToListAsync();
        }
        public async Task<List<ProjectViewModel>> GetListProject()
        {
            return _mapper.Map<List<ProjectViewModel>>(await _context.Projects.ToListAsync());
        }

        public async Task<PagedList<Article>> GetAllPaging(string keyword, int page, int pageSize)
        {
            var source = _context.Articles.AsQueryable();
            if (!keyword.IsNullOrEmpty())
            {
                source = source.Where(x => x.Name.Contains(keyword));
            }
            return await PagedList<Article>.CreateAsync(source, page, pageSize);
        }

        public async Task<Article> GetByID(int id)
        {
            return await _context.Articles.FindAsync(id);
        }

        public async Task<bool> Update(Article entity)
        {
           var item= await _context.Articles.FindAsync(entity.ID);
            item.Name = entity.Name;
            item.Description = entity.Description;
            item.Content = entity.Content;
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }
    }
}
