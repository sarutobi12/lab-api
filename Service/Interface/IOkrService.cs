﻿using Data.Models;
using Data.ViewModel.OKR;
using Data.ViewModel.OKRCategory;
using Data.ViewModel.OKRDetail;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
   public interface IOkrService
    {
        Task<List<TreeView>> GetListTree();
        Task<bool> AddOrUpdate(OC entity);
        Task<bool> IsExistsCode(int ID);
        Task<bool> Delete(int ID);
        Task<bool> Rename(TreeViewRenameOKR level);
        string GetNode(string code);
        string GetNode(int id);
        Task<IEnumerable<TreeViewOKR>> GetListTreeOC(int parentID, int id);

        Task<object> CreateOC(CreateOKRViewModel task);
        Task<object> CreateSubOC(CreateOKRViewModel task);

        Task<object> AddDetailOKR(OKRDetailViewModel entity);

        Task<List<OKRCategoryViewModel>> GetListOKRCategory();

        Task<object> GetDetail(int okrID);

        Task<object> GetListAllCategory(int okrID);
        Task<object> UpdateOKR(OKRDetailViewModel entity);
        Task<bool> DeleteOkrDetail(int ID);

    }
}
