﻿using Data.Models;
using Data.ViewModel.Project;
using Service.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
namespace Service.Interface
{

    public interface IArticleService
    {
        Task<bool> Create(Article entity);
        Task<bool> Update(Article entity);
        Task<bool> Delete(int id);
        Task<Article> GetByID(int id);
        Task<List<Article>> GetAll();
        Task<PagedList<Article>> GetAllPaging(string keyword, int page, int pageSize);
        Task<List<ProjectViewModel>> GetListProject();
    }
}
