﻿using Data.Models;
using Data.ViewModel.TeamMember;
using Service.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface ITeamService
    {
        Task<bool> Create(TeamMember entity);
        Task<bool> Update(TeamMember entity);
        Task<bool> Delete(int id);
        Task<TeamMember> GetByID(int id);
        Task<List<TeamMember>> GetAll();
        Task<PagedList<TeamMember>> GetAllPaging(string keyword,int page, int pageSize);
        Task<List<TeamMemberViewModel>> GetListMember();
    }
}
