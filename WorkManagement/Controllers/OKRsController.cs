﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Data.Models;
using Service.Interface;
using Data.ViewModel.OKR;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Data.ViewModel.OKRDetail;

namespace WorkManagement.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class OKRsController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IOCService _ocService;
        private readonly IOkrService _okrService;
        public readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuaration;
        private readonly IMapper _mapper;
        public OKRsController(DataContext context, IMapper mapper, IOkrService okrService, IOCService ocService, IWebHostEnvironment environment, IConfiguration configuaration)
        {
            _ocService = ocService;
            _context = context;
            _environment = environment;
            _configuaration = configuaration;
            _okrService = okrService;
            _mapper = mapper;
        }

        // GET: api/Projects
        [HttpGet]
        public async Task<ActionResult> GetListTree()
        {
            return Ok(await _okrService.GetListTree());
        }

        [HttpPost]
        public async Task<IActionResult> CreateOC([FromBody]CreateOKRViewModel oC)
        {
            return Ok(await _okrService.CreateOC(oC));
        }

        [HttpPost]
        public async Task<IActionResult> Add(OKRDetailViewModel entity)
        {
            return Ok(await _okrService.AddDetailOKR(entity));
        }

        [HttpGet]
        public async Task<IActionResult> GetOKRCategory()
        {
            return Ok(await _okrService.GetListOKRCategory());
        }

        [HttpGet("{okrID}")]
        public async Task<IActionResult> GetDetail(int okrID)
        {
            return Ok(await _okrService.GetDetail(okrID));
        }


        // DELETE: api/Projects/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Project>> DeleteOkr(int id)
        {
            return Ok(await _okrService.Delete(id));
        }

        [HttpGet("{okrID}")]
        public async Task<IActionResult> GetListAllCategory(int okrID)
        {
            return Ok(await _okrService.GetListAllCategory(okrID));
        }

        [HttpPost]
        public async Task<IActionResult> UpdateOKR(OKRDetailViewModel entity)
        {
            return Ok(await _okrService.UpdateOKR(entity));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<OKRDetail>> DeleteOkrDetail(int id)
        {
            return Ok(await _okrService.DeleteOkrDetail(id));
        }


        [HttpPost]
        public async Task<IActionResult> Rename([FromBody]TreeViewRenameOKR okr)
        {
            return Ok(await _okrService.Rename(okr));
        }

    }
}
