﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Data.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;

namespace WorkManagement.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        public static IWebHostEnvironment _environment;
        public ImageController(IWebHostEnvironment environment)
        {
            _environment = environment;

        }

        //public class FileUpload
        //{
        //    public IFormFile files { get; set; }
        //}
        [HttpPost]
       public async Task<IActionResult> SaveFile(IFormFile formFile)
        {
            try
            {
                IFormFile file = Request.Form.Files["formFile"];
                if (file != null)
                {
                    string filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    filename = _environment.WebRootPath + "\\Image" + $@"\{filename}";
                    if (!System.IO.File.Exists(filename))
                    {
                        using (FileStream fs = System.IO.File.Create(filename))
                        {
                            file.CopyTo(fs);
                            fs.Flush();
                        }
                    }
                    else
                    {
                        Response.Clear();
                        Response.StatusCode = 204;
                        Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File already exists.";
                    }
                }
                
            }
            catch (Exception ex)
            {
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.StatusCode = 204;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "No Content";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = ex.Message;
            }
            return Content("");
        }
        [HttpPost]
        public async Task<IActionResult> Image(IFormFile formFile)
        {
            byte[] image = null;
            IFormFile file = Request.Form.Files["UploadedFile"];
            if ((file != null) && (file.Length > 0) && !string.IsNullOrEmpty(file.FileName))
            {
                using (var stream = new MemoryStream())
                {
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\images\\" + file.FileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();
                        //return "\\images\\" + file.FileName;
                    }
                    await file.CopyToAsync(stream);
                    image = stream.ToArray();
                };
            }

            return Ok();
        }
        [HttpPost]
        public async Task<string> Post([FromForm]Project project)
        {
            //IFormFile file = Request.Form.Files["UploadedFile"];
            var file = HttpContext.Request.Form.Files["UploadedFile"];
            try
            {
                if (file != null)
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\images\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\images\\");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\images\\" + file.FileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();
                        return "\\images\\" + file.FileName;
                    }
                }
                else {
                    return "Failed";
                }
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }
        }


        // GET api/files/sample.png
        [HttpGet("{fileName}")]
        public string Get(string fileName)
        {
            string path = _environment.WebRootPath + "/image/" + fileName;
            byte[] b = System.IO.File.ReadAllBytes(path);
            return "data:image/png;base64," + Convert.ToBase64String(b);
        }


    }
}