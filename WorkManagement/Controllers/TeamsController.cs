﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Data.Models;
using Service.Interface;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;

namespace WorkManagement.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly DataContext _context;
    
        private readonly ITeamService _teamService;
        public readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuaration;

        public TeamsController(DataContext context, ITeamService teamService, IWebHostEnvironment environment, IConfiguration configuration)
        {
            _configuaration = configuration;
            _context = context;
            _teamService = teamService;
            _environment = environment;

        }

        // GET: api/Projects
        [HttpGet("{keyword}/{page}/{pageSize}")]
        public async Task<ActionResult> GetAllPaging(string keyword, int page, int pageSize)
        {
            return Ok(await _teamService.GetAllPaging(keyword, page, pageSize));
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            return Ok(await _teamService.GetAll());
        }
        

        [HttpGet]
        public async Task<ActionResult> GetListMember()
        {
            return Ok(await _teamService.GetListMember());
        }
        
        [HttpPost]
        public async Task<IActionResult> Update([FromForm]TeamMember entity)
        {

            if (ModelState.IsValid)
            {
                IFormFile file = Request.Form.Files["UploadedFile"];
                var MemberName = Request.Form["UploadedFileMemberNameEdit"];
                var JobName = Request.Form["UploadedFileJobNameEdit"];
                var Position = Request.Form["UploadedFilePositionEdit"];
                var ID = Request.Form["UploadedFileID"];
                //var item = await _context.Projects.FindAsync(entity.ID);
                if (file != null)
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\team\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\team\\");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\team\\" + file.FileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();
                        entity.ID = int.Parse(ID);
                        var item = await _context.TeamMembers.FindAsync(entity.ID);
                        item.Name = MemberName;
                        item.JobName = JobName;
                        item.Position = int.Parse(Position);
                        item.Image = _configuaration["AppSettings:applicationUrl"] + $"/team/{file.FileName}";
                        //return "\\image\\" + file.FileName;

                    }
                }
                else
                {
                    entity.ID = int.Parse(ID);
                    var item = await _context.TeamMembers.FindAsync(entity.ID);
                    item.Name = MemberName;
                    item.JobName = JobName;
                    item.Position = int.Parse(Position);
                }

                //_context.Add(entity);
                await _context.SaveChangesAsync();
                return Ok(entity);

            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }
            return Ok(entity);
            
        }

        // POST: api/Projects
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<TeamMember>> Create(TeamMember project)
        {
            return Ok(await _teamService.Create(project));
        }



        [HttpPost]
        public async Task<ActionResult<TeamMember>> Created([FromForm]TeamMember entity)
        {
            if (ModelState.IsValid)
            {
                IFormFile file = Request.Form.Files["UploadedFile"];
                var MemberName = Request.Form["UploadedFileMemberName"];
                var JobName = Request.Form["UploadedFileJobName"];
                var Position = Request.Form["UploadedFilePosition"];
                if (file != null)
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\team\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\team\\");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\team\\" + file.FileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();
                        entity.Name = MemberName;
                        entity.JobName = JobName;
                        entity.Position = int.Parse(Position);
                        //entity.CreateTime = DateTime.Now;
                        entity.Image = _configuaration["AppSettings:applicationUrl"] + $"/team/{file.FileName}";
                        //return "\\image\\" + file.FileName;

                    }
                }
                else
                {
                    entity.Name = MemberName;
                    entity.JobName = JobName;
                    entity.Position = int.Parse(Position);
                }

                _context.Add(entity);
                await _context.SaveChangesAsync();
                return Ok(entity);

            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }
            return Ok(entity);
        }
        // DELETE: api/Projects/5

        [HttpDelete("{id}")]
        public async Task<ActionResult<TeamMember>> DeleteMember(int id)
        {
            return Ok(await _teamService.Delete(id));
        }


    }
}
