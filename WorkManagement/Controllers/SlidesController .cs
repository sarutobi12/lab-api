﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Data.Models;
using Service.Interface;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;

namespace WorkManagement.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SlidesController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IProjectService _projectService;
        private readonly ISlideService _slideService;
        public readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuaration;

        public SlidesController(DataContext context, IProjectService projectService, ISlideService slideService, IWebHostEnvironment environment, IConfiguration configuration)
        {
            _configuaration = configuration;
            _context = context;
            _projectService = projectService;
            _slideService = slideService;
            _environment = environment;

        }

        // GET: api/Projects
        [HttpGet("{keyword}/{page}/{pageSize}")]
        public async Task<ActionResult> GetAllPaging(string keyword, int page, int pageSize)
        {
            return Ok(await _slideService.GetAllPaging(keyword, page, pageSize));
        }
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            return Ok(await _slideService.GetAll());
        }
        

        [HttpGet]
        public async Task<ActionResult> GetListSlide()
        {
            return Ok(await _slideService.GetListSlide());
        }
        
        [HttpPost]
        public async Task<IActionResult> Update([FromForm]Slide entity)
        {

            if (ModelState.IsValid)
            {
                IFormFile file = Request.Form.Files["UploadedFile"];
                var Title = Request.Form["UploadedFileTitleEdit"];
                var Subtitle = Request.Form["UploadedFileSubtitleEdit"];
                var ID = Request.Form["UploadedFileID"];
                //var item = await _context.Projects.FindAsync(entity.ID);
                if (file != null)
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\slide\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\slide\\");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\slide\\" + file.FileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();
                        entity.ID = int.Parse(ID);
                        var item = await _context.Slides.FindAsync(entity.ID);
                        item.Title = Title;
                        item.Subtitle = Subtitle;
                        item.Image = _configuaration["AppSettings:applicationUrl"] + $"/slide/{file.FileName}";
                        //return "\\image\\" + file.FileName;

                    }
                }
                else
                {
                    entity.ID = int.Parse(ID);
                    var item = await _context.Slides.FindAsync(entity.ID);
                    item.Title = Title;
                    item.Subtitle = Subtitle;
                }

                //_context.Add(entity);
                await _context.SaveChangesAsync();
                return Ok(entity);

            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }
            return Ok(entity);
            
        }

        // POST: api/Projects
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Slide>> Create(Slide project)
        {
            return Ok(await _slideService.Create(project));
        }



        [HttpPost]
        public async Task<ActionResult<Slide>> Created([FromForm]Slide entity)
        {
            if (ModelState.IsValid)
            {
                IFormFile file = Request.Form.Files["UploadedFile"];
                var Title = Request.Form["UploadedFileTitle"];
                var Subtitle = Request.Form["UploadedFileSubtitle"];
                if (file != null)
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\slide\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\slide\\");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\slide\\" + file.FileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();
                        entity.Title = Title;
                        entity.Subtitle = Subtitle;
                        //entity.CreateTime = DateTime.Now;
                        entity.Image = _configuaration["AppSettings:applicationUrl"] + $"/slide/{file.FileName}";
                        //return "\\image\\" + file.FileName;

                    }
                }
                else
                {
                    entity.Title = Title;
                    entity.Subtitle = Subtitle;
                }
                _context.Add(entity);
                await _context.SaveChangesAsync();
                return Ok(entity);

            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }
            return Ok(entity);
        }
        // DELETE: api/Projects/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Slide>> DeleteSlide(int id)
        {
            return Ok(await _slideService.Delete(id));
        }


    }
}
